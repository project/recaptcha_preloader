```
            ____    _    ____ _____ ____ _   _    _      ____           _                 _
  _ __ ___ / ___|  / \  |  _ \_   _/ ___| | | |  / \    |  _ \ _ __ ___| | ___   __ _  __| | ___ _ __
 | '__/ _ \ |     / _ \ | |_) || || |   | |_| | / _ \   | |_) | '__/ _ \ |/ _ \ / _` |/ _` |/ _ \ '__|
 | | |  __/ |___ / ___ \|  __/ | || |___|  _  |/ ___ \  |  __/| | |  __/ | (_) | (_| | (_| |  __/ |
 |_|  \___|\____/_/   \_\_|    |_| \____|_| |_/_/   \_\ |_|   |_|  \___|_|\___/ \__,_|\__,_|\___|_|

```

# reCAPTCHA Preloader

Provide preloader for Google reCAPTCHA v2 Checkbox.

In some cases (for example, with a slow Internet connection), the visitor may
mistakenly decide that the login page has already loaded enough. After pressing
the button to log in to the site, this does not happen because the captcha
element has not yet been displayed on the page. So, that the user does not have
such a false feeling, this module provides the following solutions:

1. Along with the form, display an element that copies the appearance of the
   captcha, which does not have a checkbox, but contains a message that warns
   that the captcha is in the process of loading.
1. The site login button is initially inactive.
1. Once the captcha is loaded, it replaces the stub element and the button
   becomes clickable.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/recaptcha_preloader).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/recaptcha_preloader).

## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the following modules:

- [reCAPTCHA](https://www.drupal.org/project/recaptcha)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the module at Administration > Extend.
1. Navigate to Administration > Configuration > People > CAPTCHA module
   settings > reCAPTCHA and check the "Enable" checkbox in the "Preloader"
   subsection of the "Widget settings" section.
1. Fill the message text of the preloader widget in the "Message" field and
   press "Save configuration".


## Maintainers

- Oleksandr Horbatiuk - [lexhouk](https://www.drupal.org/u/lexhouk)
