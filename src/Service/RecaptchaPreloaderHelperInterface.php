<?php

namespace Drupal\recaptcha_preloader\Service;

/**
 * Defines the helper service interface.
 */
interface RecaptchaPreloaderHelperInterface {

  /**
   * Disables submit form elements.
   *
   * @param array $elements
   *   The element array.
   */
  public function search(array &$elements): void;

  /**
   * Check if the required settings are set to values for using the preloader.
   */
  public function isAvailable(): bool;

  /**
   * Generates a challenge.
   */
  public function build(): array;

}
