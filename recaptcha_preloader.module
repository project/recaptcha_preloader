<?php

/**
 * @file
 * Provides preloader for Google reCAPTCHA v2 Checkbox.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function recaptcha_preloader_help(
  string $route_name,
  RouteMatchInterface $route_match
): string {
  if ($route_name !== 'help.page.recaptcha_preloader') {
    return '';
  }

  return '<h3>' . t('About') . '</h3>' .
    '<p>' . t('Provides preloader for Google reCAPTCHA v2 Checkbox.') . '</p>' .
    '<p>' . t('In some cases (for example, with a slow Internet connection), the visitor may mistakenly decide that the login page has already loaded enough. After pressing the button to log in to the site, this does not happen because the captcha element has not yet been displayed on the page. So, that the user does not have such a false feeling, this module provides the following solutions:') . '</p>' .
    '<ol>' .
    '<li>' . t('Along with the form, display an element that copies the appearance of the captcha, which does not have a checkbox, but contains a message that warns that the captcha is in the process of loading.') . '</li>' .
    '<li>' . t('The site login button is initially inactive.') . '</li>' .
    '<li>' . t('Once the captcha is loaded, it replaces the stub element and the button becomes clickable.') . '</li>' .
    '</ol>';
}

/**
 * Implements hook_form_alter().
 */
function recaptcha_preloader_form_alter(
  array &$form,
  FormStateInterface $form_state,
  string $form_id
): void {
  if ($form_state->getUserInput()) {
    return;
  }

  /** @var \Drupal\recaptcha_preloader\Service\RecaptchaPreloaderHelperInterface $helper */
  $helper = \Drupal::service('recaptcha_preloader.helper');

  if (!$helper->isAvailable()) {
    return;
  }

  $types = ['recaptcha/reCAPTCHA'];
  $default = \Drupal::config('captcha.settings')->get('default_challenge');

  if ($default === $types[0]) {
    $types[] = 'default';
  }

  $entities = ($query = \Drupal::entityQuery('captcha_point'))
    ->condition('status', TRUE)
    ->condition(
      $query->orConditionGroup()
        ->condition('label', $form_id)
        ->condition('formId', $form_id),
    )
    ->condition('captchaType', $types, 'IN')
    ->count()
    ->accessCheck()
    ->execute();

  if ($entities) {
    $helper->search($form);
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function recaptcha_preloader_form_recaptcha_admin_settings_alter(
  array &$form,
  FormStateInterface $form_state,
  string $form_id
): void {
  $form['widget']['recaptcha_preloader'] = [
    '#type' => 'details',
    '#title' => t('Preloader'),
    '#open' => TRUE,
  ];

  $config = \Drupal::config('recaptcha_preloader.settings');

  $form['widget']['recaptcha_preloader']['recaptcha_preloader_status'] = [
    '#type' => 'checkbox',
    '#title' => t('Enable'),
    '#description' => t('Disable submit form elements and show the message when reCAPTCHA is loading.'),
    '#default_value' => $config->get('status'),
  ];

  $state = [
    ':input[name="recaptcha_preloader_status"]' => [
      'checked' => TRUE,
    ],
  ];

  $form['widget']['recaptcha_preloader']['recaptcha_preloader_message'] = [
    '#type' => 'textfield',
    '#title' => t('Message'),
    '#description' => t('Special text on the reCAPTCHA place while reCAPTCHA is loading.'),
    '#default_value' => $config->get('message'),
    '#states' => [
      'enabled' => $state,
      'required' => $state,
    ],
  ];

  $form['#submit'][] = '_recaptcha_preloader_submit';
}

/**
 * Implements hook_captcha().
 */
function recaptcha_preloader_captcha(string $op, string $captcha_type = '') {
  if ($op !== 'generate' || $captcha_type !== 'reCAPTCHA') {
    return;
  }

  /** @var \Drupal\recaptcha_preloader\Service\RecaptchaPreloaderHelperInterface $helper */
  $helper = \Drupal::service('recaptcha_preloader.helper');

  return $helper->build();
}

/**
 * Implements hook_element_info_alter().
 */
function recaptcha_preloader_element_info_alter(array &$info): void {
  /** @var \Drupal\recaptcha_preloader\Service\RecaptchaPreloaderHelperInterface $helper */
  $helper = \Drupal::service('recaptcha_preloader.helper');

  if ($helper->isAvailable()) {
    array_unshift(
      $info['captcha']['#process'],
      '_recaptcha_preloader_captcha_process',
    );
  }
}

/**
 * Form submission handler for recaptcha_admin_settings().
 */
function _recaptcha_preloader_submit(
  array $form,
  FormStateInterface $form_state
): void {
  $status = $form_state->getValue('recaptcha_preloader_status');

  \Drupal::configFactory()->getEditable('recaptcha_preloader.settings')
    ->set('status', $status)
    ->set('message', $form_state->getValue('recaptcha_preloader_message'))
    ->save();

  if ($form['widget']['recaptcha_preloader']['recaptcha_preloader_status']['#default_value'] !== $status) {
    drupal_flush_all_caches();
  }
}

/**
 * Process callback for CAPTCHA form element.
 */
function _recaptcha_preloader_captcha_process(
  array &$element,
  FormStateInterface $form_state,
  array &$complete_form
): array {
  $captcha_type = &$element['#captcha_type'];

  if ($captcha_type === 'default') {
    $captcha_type = \Drupal::config('captcha.settings')->get('default_challenge')
      ?: 'captcha/Math';
  }

  if ($captcha_type === 'recaptcha/reCAPTCHA') {
    $captcha_type = 'recaptcha_preloader/reCAPTCHA';
  }

  return $element;
}
