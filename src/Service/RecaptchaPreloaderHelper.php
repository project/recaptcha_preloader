<?php

namespace Drupal\recaptcha_preloader\Service;

use Drupal\Core\Asset\AssetResolverInterface;
use Drupal\Core\Asset\AttachedAssets;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Template\Attribute;

/**
 * Defines the helper service.
 */
class RecaptchaPreloaderHelper implements RecaptchaPreloaderHelperInterface {

  /**
   * The settings of modules.
   *
   * @var \Drupal\Core\Config\ImmutableConfig[]
   */
  protected array $configs = [];

  /**
   * RecaptchaPreloaderHelper constructor.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Asset\AssetResolverInterface $assetResolver
   *   The asset resolver.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(
    protected ModuleHandlerInterface $moduleHandler,
    ConfigFactoryInterface $config_factory,
    protected AssetResolverInterface $assetResolver,
    protected LanguageManagerInterface $languageManager,
    protected RendererInterface $renderer
  ) {
    foreach (['recaptcha', 'recaptcha_preloader'] as $module) {
      $this->configs[$module] = $config_factory->get($module . '.settings');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function search(array &$elements): void {
    foreach (Element::children($elements) as $key) {
      if (is_array($elements[$key])) {
        switch ($elements[$key]['#type']) {
          case 'actions':
            $this->search($elements[$key]);
            break;

          case 'submit':
            $elements[$key]['#disabled'] = TRUE;
            $elements[$key]['#attributes']['class'][] = 'blocked-by-recaptcha';
            break;
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function isAvailable(): bool {
    if ($this->configs['recaptcha_preloader']->get('status')) {
      $size = $this->configs['recaptcha']->get('widget.size');

      // Allow using only with default options set of the widget size setting.
      if (!$size || $size === 'compact') {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $info = [
      'module' => 'recaptcha',
      'captcha_type' => 'reCAPTCHA',
    ];

    $captcha = $this->moduleHandler->invoke(
      $info['module'],
      'captcha',
      ['generate', $info['captcha_type']],
    );

    $this->moduleHandler->alter('captcha', $captcha, $info);

    if (!isset($captcha['form']['recaptcha_widget']) || !$this->isAvailable()) {
      return $captcha;
    }

    $assets = AttachedAssets::createFromRenderArray([
      '#attached' => [
        'library' => [
          'recaptcha_preloader/connector',
        ],
      ],
    ]);

    $assets = [
      'css' => [$this->assetResolver->getCssAssets($assets, FALSE)],
      'js' => $this->assetResolver->getJsAssets($assets, FALSE),
    ];

    $fields = [
      'css' => [
        'tag' => 'link',
        'attribute' => 'href',
        'attributes' => [
          'rel' => 'stylesheet',
          'type' => 'text/css',
        ],
      ],
      'js' => [
        'tag' => 'script',
        'attribute' => 'src',
        'attributes' => [
          'async' => TRUE,
          'defer' => TRUE,
        ],
      ],
    ];

    $widget = &$captcha['form']['recaptcha_widget'];
    $widget['#attached']['html_head'] = [];
    $langcode = $this->languageManager->getCurrentLanguage()->getId();

    foreach ($assets as $type => $group) {
      $id = 1;

      foreach ($group as $items) {
        foreach ($items as $item) {
          $url = $item['data'];

          if ($item['type'] === 'external') {
            $url .= '&hl=' . $langcode;
          }
          else {
            $url = '/' . $url;
          }

          $widget['#attached']['html_head'][] = [
            [
              '#tag' => $fields[$type]['tag'],
              '#attributes' => [
                $fields[$type]['attribute'] => $url,
              ] + $fields[$type]['attributes'],
            ],
            'recaptcha_preloader_' . $type . '_' . ($id++),
          ];
        }
      }
    }

    $attributes = [
      'class' => [
        'g-recaptcha-wrapper',
        $this->configs['recaptcha']->get('widget.size') ?: 'default',
        'loading',
      ],
    ];

    $widget['#prefix'] = '<div' . new Attribute($attributes) . '>';

    $element = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'class' => [
          'g-recaptcha-message',
          $this->configs['recaptcha']->get('widget.theme') ?: 'light',
        ],
      ],
      '#value' => $this->configs['recaptcha_preloader']->get('message'),
    ];

    $widget['#suffix'] .= $this->renderer->render($element) . '</div>';

    return $captcha;
  }

}
